# Taarikh

Taarikh is a field formatter and widget module to display and input hijri dates for the fields defined by the date module. The formatter and the widget works with the date fields provided by Drupal core.

This module also provides element types which you, as a developer, could use in your own forms.

## When do I need this module

You need this module for any of the use cases:

- You need to allow users to enter dates in Hijri or another calendar.
- You need to display dates in Hijri or another calendar.
- You need to define your own calendar types.

## Installation

Since this module depends on a PHP package, you must install this using composer.

_Note: It is assumed that you are using the community recommended method of using Drupal with composer using the [drupal-composer project](https://github.com/drupal-composer/drupal-project) or the recommended approach in Drupal 8.8._

```bash
composer require drupal/taarikh:^1.0
```

The date field uses the [jQuery Calendars library by Keith Wood](http://keith-wood.name/calendars.html). The module has been tested with version 2.1.0 but the latest version should work as well.

To use composer to download the library as well, follow these steps:

Add the library to composer repositories section as follows:

```json
{
    ...
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "package",
            "package": {
                "name": "kbwood/jquery.calendars",
                "type": "drupal-library",
                "version": "2.1.0",
                "dist": {
                    "url": "http://keith-wood.name/zip/jquery.calendars.package-2.1.0.zip",
                    "type": "zip"
                }
            }
        },
        ...
}
```

_You may replace the version above as you see fit._

After adding the above entry, run the following command:

```bash
Run `composer require kbwood/jquery.calendars`
```

## Usage

This module does not provide a new field type, just widgets and formatters. This means that you are free to switch the widgets and/or formatters for each field separately without losing any date. The data is stored in database as a regular Gregorian date.

This also means that you typically would want to set both widget and formatters. If you just change the widget for a content type, the user would be able to enter the date using the hijri calendar but it would be displayed using the Gregorian calendar when viewing the content. To view it using the Hijri calendar, you would also need to change the formatter.

### Using the formatter

Go to the 'Manage display' tab of any content type or entity type. For all date fields, you can now change the formatter to "Taarikh date and time". You can select the algorithm and the format from the settings.

### Using the widget

Go to the 'Manage form display' tab of any content type or entity type. For all date fields, you can now change the widget to "Taarikh date and time".

## Local Development

We use DDEV to work on this module. The DDEV configuration is ignored from git for two reasons:

1. It's unnecessary.
2. It locks in the Drupal version.

Instead, we'll use a script to set up the development environment. Fortunately, [ddev-drupal-contrib](https://github.com/ddev/ddev-drupal-contrib) makes this very easy for us. The steps mentioned there are scripted in `./setup_dev.sh` and some more. The script also lets you specify the Drupal version to use (corresponds to DDEV recipes: `drupal8`, `drupal9`, and `drupal10` as of right now). It also copies a settings.local.php and modifies settings.php to have it take effect.

Usage:

```bash
./setup_dev.sh drupal9
```

This will set up a DDEV site at the address https://taarikh-drupal9.ddev.site (if you have cloned this repo in a directory called `taarikh`). Now you can install the site and happy testing.
