<?php

namespace Drupal\taarikh\Plugin\TaarikhAlgorithm;

use Drupal\taarikh\TaarikhAlgorithmPluginInterface;

/**
 * Implementation of the Fatimid Astronomical algorithm.
 *
 * @TaarikhAlgorithm(
 *   id = "fatimid_astronomical",
 *   title = @Translation("Fatimid Astronomical"),
 *   algorithm_class = "Hussainweb\DateConverter\Algorithm\Hijri\HijriFatimidAstronomical"
 * )
 */
class FatimidAstronomical extends TaarikhAlgorithmPluginBase implements TaarikhAlgorithmPluginInterface {}
