<?php

/**
 * @file
 * Taarikh module install file.
 */

/**
 * Implements hook_requirements().
 */
function taarikh_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    // @todo This could be removed once we start using Libraries API.
    // See https://www.drupal.org/node/2895131.
    $requirements['taarikh'] = [
      'title' => \Drupal::translation()->translate('jQuery Calendars library'),
      'value' => \Drupal::translation()->translate('Unknown'),
    ];
    $requirements['taarikh']['severity'] = REQUIREMENT_OK;
    $libraries = \Drupal::service('library.discovery')->getLibrariesByExtension('taarikh');
    $library = $libraries['jquery.calendars'];

    // Check if jQuery Calendars library exists. Try to determine it's version
    // if it does.
    if (!_taarikh_requirements_installed()) {
      $message = 'The <a href="@url">@title</a> library (version @version or higher) is not installed.';
      $args = [
        '@title' => $library['title'],
        '@url' => $library['website'],
        '@version' => $library['version'],
      ];

      $requirements['taarikh']['description'] = \Drupal::translation()->translate($message, $args);
      $requirements['taarikh']['severity'] = REQUIREMENT_ERROR;
    }
    elseif (($installed_version = _taarikh_requirements_version()) === NULL) {
      $requirements['taarikh']['description'] = \Drupal::translation()->translate('jQuery Calendars version could not be determined.');
      $requirements['taarikh']['severity'] = REQUIREMENT_INFO;
    }
    elseif (!version_compare($library['version'], $installed_version, '<=')) {
      $requirements['taarikh']['description'] = \Drupal::translation()->translate('jQuery Calendars @version or higher is required.', ['@version' => $library['version']]);
      $requirements['taarikh']['severity'] = REQUIREMENT_INFO;
    }

    $requirements['taarikh']['value'] = empty($installed_version) ? \Drupal::translation()->translate('Not found') : $installed_version;
  }

  return $requirements;
}

/**
 * Checks wether jQuery Calendars library exists or not.
 *
 * @return bool
 *   TRUE if jQuery Calendars library installed, FALSE otherwise.
 */
function _taarikh_requirements_installed() {
  $libraries = \Drupal::service('library.discovery')->getLibrariesByExtension('taarikh');
  $library = $libraries['jquery.calendars'];

  // We grab the first file and check if it exists.
  if (!file_exists($library['js'][0]['data'])) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Returns the version of the installed taarikh library.
 *
 * @return string
 *   The version of installed taarikh or NULL if unable to detect version.
 */
function _taarikh_requirements_version() {
  $libraries = \Drupal::service('library.discovery')->getLibrariesByExtension('taarikh');
  $library = $libraries['jquery.calendars'];

  $file_name = $library['js'][0]['data'];
  $file_name = str_replace('.min', '', $file_name);

  // Read contents of taarikh's javascript file.
  $configcontents = @file_get_contents($file_name);
  if (!$configcontents) {
    return NULL;
  }

  // Search for version string using a regular expression.
  if (preg_match("#Calendars for jQuery.*v(\d+[\.\d+]*)#s", $configcontents, $matches)) {
    return $matches[1];
  }

  return NULL;
}
