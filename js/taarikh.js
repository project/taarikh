(($, Drupal) => {
  /**
   * Attach calendars picker on Taarikh date elements
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior.
   */
  Drupal.behaviors.taarikh = {
    attach(context) {
      once('taarikhPicker', 'input[data-taarikh-date-format]', context).forEach(
        (value) => {
          const calendarPickerSettings = {
            calendar: $.calendars.instance('fatimid_astronomical'),
            firstDay: value.getAttribute('data-taarikh-first-day'),
            showSpeed: 'fast',
          };

          // The date format is saved in PHP style, we need to convert to jQuery
          // datepicker.
          const dateFormat = value.getAttribute('data-taarikh-date-format');
          calendarPickerSettings.dateFormat = dateFormat
            .replace('y', 'yy')
            .replace('Y', 'yyyy')
            .replace('m', 'mm')
            .replace('d', 'dd')
            .replace('j', 'd')
            .replace('l', 'DD')
            .replace('n', 'm')
            .replace('F', 'MM');

          $(value).calendarsPicker(calendarPickerSettings);
        },
      );
    },
  };
})(jQuery, Drupal);
